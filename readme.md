# IAW-UD2-A7 Proyecto BatoiTube

### Introducción

El siguiente repositorio contiene una web **estática** que representa la primera versión de la aplicación **batoiTube** 
que llevaremos a cabo en diferentes partes a lo largo del curso.

Se trata de una aplicación que nos permitirá tener un resumen de los videos más interesantes de **youtube**, llevar a cabo
su seguimiento y obtener un feedback de los diferentes usuarios.

Presenta una segunda página con un formulario a través del cual, tanto usuarios como clientes, pueden establecer un primer contacto con nuestra empresa

Para poner en marcha la aplicación e ir llevando a cabo las diferentes tareas, podemos hacer uso del servidor de desarrollo 
contenido en en el propio **intérprete de php**

```bash
    $ php -S localhost:8001
```

### Vista Previa

- La **página principal** presenta el listado de videos más vistos de **Youtube** seleccionados por nuestro propio equipo y extraidos
mediante la **API pública de Youtube**.

![Página Principal](/assets/img/article-list-preview.png)

- La **página de contacto** proporciona un medio de comunicación con nuestra empresa para los usuarios de la web, 
ya sea para establecer un primer contacto comercial (publicitar un video) o para problemas acaecidos durante la navegación.

![Página Principal](/assets/img/contact-preview.png)

### A) Creación de una web dinámica mediante vistas parciales (partials)
  
A partir de las páginas web estáticas proporcionadas en las que la ``cabecera``, ``pie``, ``menu`` y demás componentes se encuentran
duplicados:

- Pagina principal ``index.html``
- Pàgina de contacto ``contact-html``

Crea una **página web dinámica** que evite las duplicidades a partir de su división en vistas parciales (partials). Para ello deberás:
1. Crear un directorio en el raiz de la aplicación llamado ``partials`` con los siguientes ficheros:
 
**Vistas parciales comunes** 
   - ``partial_base-header.php``: Contiene las declaraciones html, cabeceras e includes.
   - ``partial_navigation-bar.php``: Contiene la barra de navegación.
   - ``partial_footer.php``: Contiene el pie de página.
   - ``partial_base-footer.php``: Contiene las etiquetas de cierre de la página html y los link a los scripts.

**Vistas parciales página listado artículos** 
   - ``partial_article-list.php``: Contiene el listado de articulos de la página principal.
   - ``partial_article-item.php``: Contiene cada uno de los artículos del listado.

**Vistas parciales página contacto** 
   - ``partial_contact.php``: Contiene el formulario de contacto.
   - ``partial_article-highlight.php``: Artículo más votado
    
2. Crea 2 páginas web dinámicas a partir de la composición de los partials creados en el paso anterior
   - ``index.php`` Representa la página principal con el listado de artículos.
   - ``contact.php`` Representa la página de contacto.

![Composicion páginas](/assets/img/partials-composition.png)

~~~
Recuerda que deberás cambiar todos los enlaces en la barra de navegación para que funcione correctamente
~~~

3. Como podemos observar, solo disponemos de un **partial** que representará la barra de navegación, no obstante, deberemos
 marcar como activo un item del menu en función de la página en la que nos encontremos.
 Para ello crearemos una variable ``$selectedMenu`` antes de la inclusión del partial que representa la barra de navegación cuyo contenido
 leeremos desde el partial y que nos servirá para saber que item de menu marcar como activo.


```php
<?php

    $selectedMenu = "index";
    include __DIR__ . "/partial_navigation-bar.php";
    
```

### B) Creación de contenido dinámico a partir de Base de datos en memoria

- Los datos de cada uno de los artículos se generarán de forma dinámica a partir de los datos contenidos presentes el array
``data/data.php``

```php
<?php

    include __DIR__ . "/data/data.php";

    foreach ($listadoVideos as $video) {
        
        include __DIR__ . "/partial_article-item.php";
    
    }
    
```
Aquí tienes las correspondencias entre los elementos del article item y el array de datos 

| Elemento de articulo | elemento del array de datos |
| ---------- | ---------- |
| img   | snippet -> thumbnails -> medium -> url |
| h2   | snippet -> title   |
| p   | snippet -> description   |
| a   | "https://www.youtube.com/watch?v=" + id -> videoId   |

### C) Validación y procesamiento del formulario

Refactoriza la página ``contacto.php`` para que cumpla con los siguientes requisitos:

- Deberá tener en cuenta las posibles entradas de datos filtrándolos de acuerdo a las siguientes reglas:

Parámetro | Tipo de datos |  Otras consideraciones
-- | -- | --
nombre | String | Requerido
pais | String | Requerido y perteneciente al conjunto ["españa","italia","alemania","francia","suecia","finlandia","estados unidos"]
email | String | email
mensaje | String | entre 10 y 100 caracteres

- Además de la validación en el servidor (código php), deberás hacerlo en el cliente mediante la utilización de los tipos
de input correspondiente a cada dato **(email, text)** y los validadores de **html5 (min, max)**

- Los datos del formularios se enviarán por `post` a la misma página. Si todo ha ido bien debe mostrar una tabla con todos 
 los datos enviados.