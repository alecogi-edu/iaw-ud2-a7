<?php
$listadoVideos = [
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/EddsML2f2o_y3mtOZHDBrdG6uZk",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "yS4ccBFViSU"
        ],
        "snippet" => [
            "publishedAt" => "2019-02-07T14=>11=>43.000Z",
            "channelId" => "UCQQsWvlIbCgxtmozcFCZyLA",
            "title" => "Cementerio Maldito - trailer 2 HD",
            "description" => "Estreno en cines 4 de abril Basado en la aterradora novela de Stephen King.",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/yS4ccBFViSU/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/yS4ccBFViSU/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/yS4ccBFViSU/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Andes Films",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/rwh947Oh1toH3HD0VlSKC9qwGNA",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "Horv5plfi7A"
        ],
        "snippet" => [
            "publishedAt" => "2017-12-01T09=>48=>12.000Z",
            "channelId" => "UCOVKRn337ZeDhwDHm26damw",
            "title" => "GLAVA® ROBUST – lett å jobbe med for kompakte tak",
            "description" => "GLAVA® ROBUST er et nytt produkt for optimal isolering, og markedets letteste løsning for kompakte tak. Alt om isolasjon=> www.glava.no GLAVA® - Isolasjon for ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/Horv5plfi7A/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/Horv5plfi7A/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/Horv5plfi7A/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "GLAVA",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/m36tvL90EL5Ojz56iUXIbDnGA-k",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "Yy2Cc5zBkKQ"
        ],
        "snippet" => [
            "publishedAt" => "2019-04-23T13=>15=>16.000Z",
            "channelId" => "UCQQsWvlIbCgxtmozcFCZyLA",
            "title" => "Proyecto Géminis - Trailer 1 (HD)",
            "description" => "Estreno en cines - 10 de octubre Gemini Man es un innovador thriller de acción protagonizado por Will Smith como Henry Brogan, un asesino de élite, que de ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/Yy2Cc5zBkKQ/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/Yy2Cc5zBkKQ/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/Yy2Cc5zBkKQ/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Andes Films",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/RZvg9lxnzOtn6sYXFTP6GxT6a3U",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "p_6D-Koeap8"
        ],
        "snippet" => [
            "publishedAt" => "2018-02-21T18=>47=>57.000Z",
            "channelId" => "UCiul5KxO4KJgNPn_FjVoVxg",
            "title" => "2018 Entry: Why I Think Neuroscience Is...™ Cool Abdullah Umar",
            "description" => "\"The Neuroscience of Memory\" Video entry for the 2018 Neuro Film Festival www.neurofilmfestival.com Learn more about the Neuro Film Festival Facebook=> ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/p_6D-Koeap8/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/p_6D-Koeap8/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/p_6D-Koeap8/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "neurofilmfest",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/dVtZQEV_dFyKu2j7TWbgSS0quHI",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "iZQgwHhtuhQ"
        ],
        "snippet" => [
            "publishedAt" => "2019-10-02T20=>08=>18.000Z",
            "channelId" => "UCQQsWvlIbCgxtmozcFCZyLA",
            "title" => "Proyecto Géminis - Next gen spot 30&quot;",
            "description" => "Estreno en cines 10 de octubre.",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/iZQgwHhtuhQ/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/iZQgwHhtuhQ/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/iZQgwHhtuhQ/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Andes Films",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/39efEJ3xkMB80JWYReMG_yhbBt8",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "tWclmXh5ZcI"
        ],
        "snippet" => [
            "publishedAt" => "2017-05-22T14=>16=>05.000Z",
            "channelId" => "UCyexqu6nPtdn_OEV_AYGZEA",
            "title" => "Estrategias diversificadas",
            "description" => "Created using PowToon -- Free sign up at http://www.powtoon.com/youtube/ -- Create animated videos and animated presentations for free. PowToon is a free ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/tWclmXh5ZcI/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/tWclmXh5ZcI/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/tWclmXh5ZcI/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Qetzalli Sánchez",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/RYIVMplJgiFTjy_OL2sNaHIOCVI",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "xNaViFkmfmc"
        ],
        "snippet" => [
            "publishedAt" => "2019-03-19T14=>18=>09.000Z",
            "channelId" => "UCqCob9pU7lzKfhB5JZEEi1A",
            "title" => "#Flow - ¡Hoy en cines!",
            "description" => "SABIA USTED QUÉ… FLOW permanece en cartelera sólo si van 100 espectadores por función el primer fin de semana. Regale una entrada de FLOW a un ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/xNaViFkmfmc/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/xNaViFkmfmc/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/xNaViFkmfmc/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Jirafa Play",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/14I383fS00GDlQUYL90PWjQgecg",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "63wIGQ2N5dg"
        ],
        "snippet" => [
            "publishedAt" => "2019-11-02T21=>32=>57.000Z",
            "channelId" => "UCH1We25Nn29nXrH3zo_26GA",
            "title" => "Miradas al cine sueco contemporáneo : ciclo de cine",
            "description" => "Del 6 al 12 de noviembre 2019.",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/63wIGQ2N5dg/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/63wIGQ2N5dg/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/63wIGQ2N5dg/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Cátedra Ingmar Bergman UNAM",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/OoDm1KjKTZrXrrEVNnbHVAWCYho",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "e7AtDwnJZHg"
        ],
        "snippet" => [
            "publishedAt" => "2018-10-01T12=>43=>14.000Z",
            "channelId" => "UCQQsWvlIbCgxtmozcFCZyLA",
            "title" => "ROCKETMAN",
            "description" => "En cines MAYO 2019.",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/e7AtDwnJZHg/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/e7AtDwnJZHg/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/e7AtDwnJZHg/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Andes Films",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/NcRBZsUytHZjJUdjekWLH_4S_yo",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "h-Nrw3RCzYo"
        ],
        "snippet" => [
            "publishedAt" => "2019-11-04T05=>05=>00.000Z",
            "channelId" => "UC2IfeSyqkIBGOBmoYFLEY3A",
            "title" => "JUGANDO FREE FIRE EN DIRECTO CON SUBS",
            "description" => "DURecorder #FreeFire #live link de la escuadra ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/h-Nrw3RCzYo/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/h-Nrw3RCzYo/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/h-Nrw3RCzYo/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Dest Epic Banner",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/ytWLYf5mCnYWttVQYO5nD1Y5OzU",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "5gx4l8YN3l4"
        ],
        "snippet" => [
            "publishedAt" => "2014-10-28T18=>35=>38.000Z",
            "channelId" => "UCX1v04kEs7wi88GO9gNt0xg",
            "title" => "Interturis - Sánchez (Salmón)",
            "description" => "Sánchez se ganó un viaje. Seguí las vacaciones de Sánchez en=> http://www.facebook.com/interturis.",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/5gx4l8YN3l4/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/5gx4l8YN3l4/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/5gx4l8YN3l4/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Interturis",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/fPrL_d_wGAYj9BDkfFyYCiHy_wU",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "wmwkQ99MbOo"
        ],
        "snippet" => [
            "publishedAt" => "2019-02-12T17=>03=>01.000Z",
            "channelId" => "UCQQsWvlIbCgxtmozcFCZyLA",
            "title" => "Yesterday – Trailer Internacional (Universal Pictures) HD",
            "description" => "En cines 12 de septiembre Síguenos en Facebook http=>//www.facebook.com/AndesFilmsChile Síguenos en Instagram en ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/wmwkQ99MbOo/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/wmwkQ99MbOo/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/wmwkQ99MbOo/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Andes Films",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/ttLMj0DHLi36c0gUAGE2OmC-Luc",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "qeAk1pN_60Y"
        ],
        "snippet" => [
            "publishedAt" => "2016-11-18T01=>42=>58.000Z",
            "channelId" => "UCB3YZAhwbsajMshO2HMdxBg",
            "title" => "Fantastic Bugs and Where to Find Them | Yale Medical School | Fantastic Beasts Parody",
            "description" => "Made for the Yale School of Medicine 2016 Hunger and Homelessness Charity Auction. Check out our other Yale Med Films class of 2019 projects=> Party in the ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/qeAk1pN_60Y/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/qeAk1pN_60Y/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/qeAk1pN_60Y/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Yale Med Films",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/2lSN_OMAurL6OTG6QknOVPrAVH0",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "poOfJJsrQhA"
        ],
        "snippet" => [
            "publishedAt" => "2019-06-26T14=>08=>29.000Z",
            "channelId" => "UC9sBtJjxGEIEJ1wPQs3C5EA",
            "title" => "MIDSOMMAR - In Theatres July 3 (15s)",
            "description" => "A film by Ari Aster (A24). Starring Florence Pugh, Will Poulter & Jack Reynor. Dani and Christian are a young American couple with a relationship on the brink of ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/poOfJJsrQhA/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/poOfJJsrQhA/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/poOfJJsrQhA/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Entract Films",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/v8e7iUoEbWe7TGtwWbiY136IHBw",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "jJchadoA7Kc"
        ],
        "snippet" => [
            "publishedAt" => "2014-11-24T12=>19=>02.000Z",
            "channelId" => "UC4fFv5YIMj3r1ygg-2i5zlQ",
            "title" => "Giralda Center, escuela de lenguas en Sevilla",
            "description" => "Así somos nosotros, así es nuestra escuela de lenguas en Sevilla (España) El vídeo se rodó durante el mes de junio de 2014. Mil gracias a nuestros alumnos ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/jJchadoA7Kc/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/jJchadoA7Kc/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/jJchadoA7Kc/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "Giralda Center",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/dYC4YXw2KndY6HHaTeYYibuWIvM",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "grJo7M52vRM"
        ],
        "snippet" => [
            "publishedAt" => "2019-08-05T14=>20=>59.000Z",
            "channelId" => "UCKkZwgZkwzjJWE1WVwaLMIg",
            "title" => "Juntos por Nicaragua=> De un pequeño bocado a comerse el mundo",
            "description" => "Puede una simple rosquilla cambiar la vida de toda una comunidad? ¿Puede un delicioso dulce tradicional traspasar fronteras? Cuando no se entiende de ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/grJo7M52vRM/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/grJo7M52vRM/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/grJo7M52vRM/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "UEenNicaragua",
            "liveBroadcastContent" => "none"
        ]
    ],
    [
        "kind" => "youtube#searchResult",
        "etag" => "j6xRRd8dTPVVptg711_CSPADRfg/goixkKp2kxK8Se6v4zCcERg9HK8",
        "id" => [
            "kind" => "youtube#video",
            "videoId" => "UX-831TyzDw"
        ],
        "snippet" => [
            "publishedAt" => "2016-10-25T19=>58=>36.000Z",
            "channelId" => "UCXJmNBwfRY-qyj0CIpVH-WA",
            "title" => "ERs Become Dumping Ground for Nation&#39;s Psychiatric Patients",
            "description" => "The nation's dwindling mental health resources are contributing significantly to increased wait times and longer emergency department stays for patients having ...",
            "thumbnails" => [
                "default" => [
                    "url" => "https://i.ytimg.com/vi/UX-831TyzDw/default.jpg",
                    "width" => 120,
                    "height" => 90
                ],
                "medium" => [
                    "url" => "https://i.ytimg.com/vi/UX-831TyzDw/mqdefault.jpg",
                    "width" => 320,
                    "height" => 180
                ],
                "high" => [
                    "url" => "https://i.ytimg.com/vi/UX-831TyzDw/hqdefault.jpg",
                    "width" => 480,
                    "height" => 360
                ]
            ],
            "channelTitle" => "American College of Emergency Physicians",
            "liveBroadcastContent" => "none"
        ]
    ]
];